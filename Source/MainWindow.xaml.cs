﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
//using System.Windows.Input;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Utilities;

namespace Test1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //KeyboardListener gkh = new KeyboardListener();
        globalKeyboardHook gkh = new globalKeyboardHook();
        List<string> lst = new List<string>();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            AddDictionary();
            // gkh.KeyDown += new KeyEventHandler(gkh_KeyDown);
            gkh.KeyUp += new KeyEventHandler(gkh_KeyUp);
        }

        private void AddDictionary()
        {
           

            gkh.HookedKeys= Enum.GetValues(typeof(Keys)).Cast<Keys>() .ToList();
           
            

            //gkh.HookedKeys.Add(Keys.A);
            //gkh.HookedKeys.Add(Keys.B);
            //gkh.HookedKeys.Add(Keys.C);
            //gkh.HookedKeys.Add(Keys.D);
            //gkh.HookedKeys.Add(Keys.E);
            //gkh.HookedKeys.Add(Keys.F);
            //gkh.HookedKeys.Add(Keys.G);
            //gkh.HookedKeys.Add(Keys.H);
            //gkh.HookedKeys.Add(Keys.I);
            //gkh.HookedKeys.Add(Keys.J);
            //gkh.HookedKeys.Add(Keys.K);
            //gkh.HookedKeys.Add(Keys.L);
            //gkh.HookedKeys.Add(Keys.M);
            //gkh.HookedKeys.Add(Keys.N);
            //gkh.HookedKeys.Add(Keys.O);
            //gkh.HookedKeys.Add(Keys.P);
            //gkh.HookedKeys.Add(Keys.Q);
            //gkh.HookedKeys.Add(Keys.R);
            //gkh.HookedKeys.Add(Keys.S);
            //gkh.HookedKeys.Add(Keys.T);
            //gkh.HookedKeys.Add(Keys.U);
            //gkh.HookedKeys.Add(Keys.V);
            //gkh.HookedKeys.Add(Keys.W);
            //gkh.HookedKeys.Add(Keys.X);
            //gkh.HookedKeys.Add(Keys.Y);
            //gkh.HookedKeys.Add(Keys.Z);
            
            //gkh.HookedKeys.Add(Keys.Return);
            //gkh.HookedKeys.Add(Keys.Back);
            //gkh.HookedKeys.Add(Keys.Space);

        }

        private void gkh_KeyUp(object sender, KeyEventArgs e)
        {
            string keytext = string.Empty;
            if (e.KeyData.ToString() == "Space")
                keytext += " ";
            else if (e.KeyData.ToString() == "Return")
                keytext += "\n";
            else if (e.KeyData.ToString().Contains("NumPad"))
                keytext += e.KeyData.ToString().Replace("NumPad","");
            else if (e.KeyData.ToString().Contains("LShiftKey"))
                keytext += e.KeyData.ToString().Replace("LShiftKey", "");
            else if (e.KeyData.ToString().Contains("RShiftKey"))
                keytext += e.KeyData.ToString().Replace("RShiftKey", "");
            else if (e.KeyData.ToString().Contains("LControlKey"))
                keytext += e.KeyData.ToString().Replace("LControlKey", "");
            else if (e.KeyData.ToString().Contains("RControlKey"))
                keytext += e.KeyData.ToString().Replace("RControlKey", "");
            else if (e.KeyData.ToString().Contains("Escape"))
                keytext += e.KeyData.ToString().Replace("Escape", "");
            else
                keytext += e.KeyData.ToString() + "";


            if (e.KeyData.ToString() == "Back")
            {
                if (lst.Count > 0)
                    lst.RemoveAt(lst.Count - 1);
            }
            else
            {
                lst.Add(keytext);
            }
            txtlog.Text = string.Join("", lst);
        }

        private void gkh_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData.ToString() == "Back")
            {
                if (lst.Count > 0)
                    lst.RemoveAt(lst.Count - 1);
            }
            //if (e.KeyData.ToString() == "Back")
            //{
            //    if (!string.IsNullOrEmpty(txtlog.Text))
            //        txtlog.Text = txtlog.Text.Remove(txtlog.Text.Length - 1, 1);
            //}
        }
    }
}
